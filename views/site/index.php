<h2>Cube Summation by César Guerrero R.</h2>
<p><strong>Para el ingreso tener en cuenta:</strong></p>
<p>
The first line contains an integer T, the number of test-cases. T testcases follow.<br> 
For each test case, the first line will contain two integers N and M separated by a single space.<br>
N defines the N * N * N matrix.<br>
M defines the number of operations.<br>
The next M lines will contain either<br>
</p>
<p>
There are two types of queries.<br>
<i>UPDATE x y z W</i><br>
updates the value of block (x,y,z) to W.<br>
<i>QUERY x1 y1 z1 x2 y2 z2</i><br>
calculates the sum of the value of blocks whose x coordinate is between x1 and x2 (inclusive), y coordinate between y1 and y2 (inclusive) and z coordinate between z1 and z2 (inclusive).</p>
<p><strong>Constrains</strong></p>
<pre>
1 <= T <= 50 
1 <= N <= 100 
1 <= M <= 1000 
1 <= x1 <= x2 <= N 
1 <= y1 <= y2 <= N 
1 <= z1 <= z2 <= N 
1 <= x,y,z <= N 
-10^9 <= W <= 109</pre>
<p><strong>Ejemplo de Ingreso</strong></p>
<pre>2
4 5
UPDATE 2 2 2 4
QUERY 1 1 1 3 3 3
UPDATE 1 1 1 23
QUERY 2 2 2 4 4 4
QUERY 1 1 1 3 3 3
2 4
UPDATE 2 2 2 1
QUERY 1 1 1 1 1 1
QUERY 1 1 1 2 2 2
QUERY 2 2 2 2 2 2</pre>
<p><strong>Ejemplo de Resultado</strong></p>
<pre>4
4
27
0
1
1</pre>

<p><strong>Input</strong></p>
<form  action="" id="cube-sum-form" method="POST">
<textarea type="text" name="consult" placeholder="Ingrese la consulta a realizar..." rows="10" cols="30" style="width:100%;"><?php if($consult) echo $consult; ?></textarea>
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
<input type="submit" value="Enviar" style="display:block; margin:0 auto; "/>
</form>


<?php

if($model)
{
?>
    <h3>Resultado de Cube Summation</h3>
    <div id="resultado">
    <?php
        echo $model;
    }
    ?>
    </div>