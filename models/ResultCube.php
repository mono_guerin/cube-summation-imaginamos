<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ResultCube is the model behind the Cube Summation.
 */
class ResultCube extends Model
{

    public $consult;
    public $lineas = array();
    public $t = 0;
    public $n;
    public $m;
    public $lineaActual;
    public $result = "";
    public $matriz=array();
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // consult are required
            [['consult'], 'required'],
        ];
    }

    //funcion que inicializa la matriz a trabajar en 0 
    //segun el tamaño dado
    function initialize(){

        //Destruye la variable
        $this->matriz = NULL;
        $this->matriz = array();


        //Inicializacion de la matriz
        for($x=0; $x<$this->n; $x++)
        {
            for($y=0; $y<$this->n; $y++)
            {
                for($z=0; $z<$this->n; $z++)
                {
                    $this->matriz[]=array(
                        $x+1,
                        $y+1,
                        $z+1,
                        0,
                         );

                }

            }
            
        }

        return "ok";

    }


    //Funcion que realiza sumatoria de un rango dentro de la matriz
    //dado por los valores x1,x2,y1,y2,z1,z2
    //y muestra el valor resultante como sum
    function query($x1,$y1,$z1,$x2,$y2,$z2){

        //global $matriz;
        $sumatoria=0;

        $tamanoMatriz = $this->n*$this->n*$this->n;
        for($i=0; $i<$tamanoMatriz; $i++)
        {
            $xActual = $this->matriz[$i][0];
            $yActual = $this->matriz[$i][1];
            $zActual = $this->matriz[$i][2];

            if($xActual>=$x1 && $xActual<=$x2 && $yActual>=$y1 && $yActual<=$y2 && $zActual>=$z1 && $zActual<=$z2)
            {   
                $value = $this->matriz[$i][3];
                $sumatoria=$value+$sumatoria;
            }
        }
        return strval($sumatoria);
    }


    //Funcion que cambia el valor de alguna posicion dentro de la matriz
    //dada la cordenada xbuscar, ybuscar, zbuscar
    //y el valor a asignar newvalue
    function update($xbuscar,$ybuscar,$zbuscar,$newvalue){

        for($i=0; $i<($this->n*$this->n*$this->n); $i++)
        {
            if($this->matriz[$i][0]==$xbuscar && $this->matriz[$i][1]==$ybuscar && $this->matriz[$i][2]==$zbuscar)
            {   
                $this->matriz[$i][3]=$newvalue;
            }
        }
        return "ok";
    }


    public function checkOperation($linea){

        $linea = explode(" ", $linea);

        //Modelo  
        //1. UPDATE x y z W
        //2. QUERY  x1 y1 z1 x2 y2 z2 
        switch ($linea[0]) {
            case 'QUERY':
            case 'query':
                
                /* Constrains a tener en cuenta
                1 <= x1 <= x2 <= N 
                1 <= y1 <= y2 <= N 
                1 <= z1 <= z2 <= N 
                */
                if (count($linea)==7)
                {
                    
                    $x1=trim($linea[1]);
                    $y1=trim($linea[2]);
                    $z1=trim($linea[3]);
                    $x2=trim($linea[4]);
                    $y2=trim($linea[5]);
                    $z2=trim($linea[6]);
                    $tamanoMatriz = $this->n;

                    if($x1<=$x2 && $x1>=1 && $x2<=$tamanoMatriz && $y1<=$y2 && $y1>=1 && $y2<=$tamanoMatriz && $z1<=$z2 && $z1>=1 && $z2<=$tamanoMatriz)
                    {
                        return $this->query($x1,$y1,$z1,$x2,$y2,$z2);
                    }
                    else
                    {
                        return "error";
                    }

                }
                else
                {
                    return "error";
                }
                break;

            case 'UPDATE':
            case 'update':
                /*Constrains para el update
                1 <= x,y,z <= N 
                -10^9 <= W <= 109
                */
                if(count($linea)==5)
                {
                    $x=trim($linea[1]);
                    $y=trim($linea[2]);
                    $z=trim($linea[3]);
                    $w=trim($linea[4]);
                    if($x>=1 && $x<=$this->n && $y>=1 && $y<=$this->n && $z>=1 && $z<=$this->n && $w>=(-pow(10,9)) && $w<=(pow(10,9)))
                    {
                        $this->update($x,$y,$z,$w);
                    }
                    else
                    {
                        return "error";
                    }
                }
                else
                {
                    return "error";
                }
                break;
            
            default:
                return "error";
                break;
        }

    }

    public function checkFormat()
    {
        if($this->consult)
        {
            $lineaActual=0; 

            $this->lineas = explode(PHP_EOL, $this->consult);

            $totalLineas = count($this->lineas);

            if($totalLineas <= 2)
            {
                return "error en la entrada, revise el formato";
            }

            $primeralinea = trim($this->lineas[$lineaActual]);

            //first constrain
            //1 <= T <= 50 
            if($primeralinea >= 1 && $primeralinea<=50)
            {
                $this->t = $primeralinea;

                $lineaActual++;

                for ($cont=0; $cont < $primeralinea; $cont++) { 

                    $segundalinea = trim($this->lineas[$lineaActual]);

                    $segundalinea = explode(" ", $segundalinea);
                    //return $segundalinea;
                    //second Constrain
                    //1 <= N <= 100 
                    if($segundalinea[0]>= 1 && $segundalinea[0]<=100)
                    {
                        $this->n = $segundalinea[0];
                        //third Constrain
                        //1 <= M <= 1000 
                        if($segundalinea[1]>= 1 && $segundalinea[1]<=1000)
                        {
                            //M es el numero de operaciones siguientes
                            $this->m = $segundalinea[1];

                            /* Revisar proximas operaciones */
                            $totalOperaciones =$this->m;
                            $totalOperaciones+=$lineaActual+1;

                            if($totalLineas<$totalOperaciones)
                            {
                                return "error cantidad de operaciones recibidas";
                            }

                            $lineaActual++;
                            //M operaciones
                            if($this->initialize() == "ok")
                            {
                                for ($i=0; $i < $this->m; $i++) { 

                                        $retornoFuncion = $this->checkOperation($this->lineas[$lineaActual+$i]);

                                        if($retornoFuncion == "error")
                                        {
                                            return "error linea ".$lineaActual+$i;
                                        }
                                        if($retornoFuncion!="")
                                            $this->result.=$retornoFuncion."<br>";
                                }
                                $lineaActual +=$this->m; 

                            }
                            else
                            {
                                return "error al inicializar la matrix";
                            }

                        }
                        else
                        {
                            return "error linea ".$lineaActual;
                        }

                    }
                    else
                    {
                        return "error linea ".$lineaActual;
                    }
                }
                return $this->result;
                
            }
            else
            {
                 return "error linea ".$lineaActual;
            }
            
            return "error general";

        }
        else
        {
            return false;
        }
        # code...
    }


}
